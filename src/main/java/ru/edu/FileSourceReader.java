package ru.edu;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Class for reading and analyzing text from source file.
 */
public class FileSourceReader implements SourceReader {

    /**
     * File to read.
     */
    private File file;

    /**
     * Setting up of the source file.
     * @param source Path to source file
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException("Source is null.");
        }

        file = new File(source);

        if (!file.exists()) {
            throw new IllegalArgumentException("Source file is missing.");
        }

    }

    /**
     * Method for reading and analizing the source file.
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    @Override
    public TextStatistics readSource(final TextAnalyzer analyzer) {
        if (analyzer == null) {
            throw new IllegalArgumentException("Analizer is null.");
        }

        try (
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr);
        ) {
            processFile(analyzer, br);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return analyzer.getStatistic();
    }

    /**
     * Local method for reading and processing text file.
     *
     * @param analyzer TextAnalyzer object
     * @param br BufferedReader object
     * @throws IOException
     */
    private void processFile(
            final TextAnalyzer analyzer,
            final BufferedReader br
    ) throws  IOException {
            String line;
            boolean firstLine = true;

            while ((line = br.readLine()) != null) {
                analyzer.analyze(!firstLine ? "\n" + line : line);
            }

            br.readLine();

    }

}
