package ru.edu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Class for saving statistical information about the text to file.
 */
public class SimpleStatisticReporter implements StatisticReporter {

    /**
     * File for storing statistics.
     */
    private File file;

    /**
     * Class constructor.
     * @param filePath path to file for storing statistics
     */
    public SimpleStatisticReporter(final String filePath) {
        file = new File(filePath);
    }

    /**
     * Saving statistical information about the text to file.
     * @param statistics object of TextStatistics class
     */
    @Override
    public void report(final TextStatistics statistics) {

        try (FileOutputStream os = new FileOutputStream(file);
            OutputStreamWriter sw = new OutputStreamWriter(
                                            os,
                                            StandardCharsets.UTF_8);
            PrintWriter writer = new PrintWriter(sw)) {

            writer.println("Words: "
                    + statistics.getWordsCount());
            writer.println("Chars: "
                    + statistics.getCharsCount());
            writer.println("Chars without spaces: "
                    + statistics.getCharsCountWithoutSpaces());
            writer.println("Punctuation chars: "
                    + statistics.getCharsCountOnlyPunctuations());
            writer.println("Simple Top10 list of words: "
                    + statistics.getTopWords());
            writer.println("Top10 list of words: "
                    + statistics.getTop10Words());
            writer.println("Top10 list of words without prepositions: "
                    + statistics.getTop10WordsWithoutPrepositions());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
