package ru.edu;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class SimpleTextAnalyzer implements TextAnalyzer {

    /**
     * Object for storing statistical information about the text.
     */
    private TextStatistics statistics = new TextStatistics();

    /**
     * Method for getting statistical information about text from the line.
     * Method:
     * - get the number of all words
     * - get the number of all characters
     * - get the number of all characters without all kind of spaces
     * - get the number of punctuation marks
     * - get list of all words in lower case,
     * sorted in descending order of occurrence in a string
     * @param line text line
     */
    @Override
    public void analyze(final String line) {

        statistics.addCharsCount(line.length());

        Map<String, Integer> map = statistics.getWordsMap();
        StringBuffer tempWord = new StringBuffer("");

        char current;
        boolean isWord = false;

        for (int i = 0; i < line.length(); i++) {
            current = line.charAt(i);
            if (Character.isSpaceChar(current)) {
                if (isWord) {
                    statistics.addWordsCount(1);
                    map.computeIfPresent(
                            String.valueOf(tempWord),
                            (s, j) -> j + 1);
                    map.putIfAbsent(String.valueOf(tempWord), 1);
                    tempWord.delete(0, tempWord.length());
                }
                isWord = false;
                continue;
            }
            statistics.addCharsCountWithoutSpaces(1);

            if (Character.isLetterOrDigit(current)) {
                isWord = true;
                tempWord.append(Character.toLowerCase(current));
            } else if (current != '\n') {
                statistics.addCharsCountOnlyPunctuations(1);
            }
        }

        if (isWord) {
            statistics.addWordsCount(1);
            map.computeIfPresent(
                    String.valueOf(tempWord),
                    (s, j) -> j + 1);
            map.putIfAbsent(String.valueOf(tempWord), 1);
        }

        statistics.addAllSortedWords(sortMapCollection(map));
    }

    private List<Map.Entry<String, Integer>> sortMapCollection(
            final Map<String, Integer> map) {

        List<Map.Entry<String, Integer>> entries =
                new ArrayList<>(map.entrySet());
        Comparator<Map.Entry<String, Integer>> compValue =
                Map.Entry.comparingByValue();
        Comparator<Map.Entry<String, Integer>> compKey =
                Map.Entry.comparingByKey();
        entries.sort(compKey);
        entries.sort(compValue.reversed());

        return entries;

    }

    /**
     * Get statistical information about the text.
     * @return TextStatistic object
     */
    @Override
    public TextStatistics getStatistic() {
        return statistics;
    }



}
