package ru.edu;

/**
 * Interface for reading the source.
 */
public interface SourceReader {

    /**
     * Setting up the source.
     * @param source
     */
    void setup(String source);

    /**
     * Method for analyzing the source.
     * @param analyzer - analyzing logic
     * @return - calculated statistics
     */
    TextStatistics readSource(TextAnalyzer analyzer);

}
