package ru.edu;

/**
 * Interface for generating a report with the received statistics.
 */
public interface StatisticReporter {

    /**
     * Generating the report.
     * @param statistics - statistics data
     */
    void report(TextStatistics statistics);
}
