package ru.edu;

/**
 * Line-by-line text analyzer.
 * All data will be set in TextStatistics object.
 * Statistics have to be updated with each call of analyze method.
 */
public interface TextAnalyzer {

    /**
     * Analizing string line.
     * @param line
     */
    void analyze(String line);

    /**
     * Get saved statistics data.
     * @return TextStatistic
     */
    TextStatistics getStatistic();
}
