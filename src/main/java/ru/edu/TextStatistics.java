package ru.edu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

/**
 * Class for storing statistical information about the text.
 */
public class TextStatistics {

    /**
     * Number of words to show from sorted list.
     */
    private static final int NUMBER_OF_WORDS = 10;

    /**
     * Total number of words in the text.
     */
    private long wordsCount;

    /**
     * Total number of characters in the text.
     */
    private long charsCount;

    /**
     * Total number of characters in the text without all kinds of spaces.
     */
    private long charsCountWithoutSpaces;

    /**
     * Total number of punctuation marks.
     */
    private long charsCountOnlyPunctuations;

    /**
     * Map of words if the text with number of occurrence.
     */
    private Map<String, Integer> map = new HashMap<>();

    /**
     * Sorted list of words of the text in descending order of occurrence.
     */
    private List<Map.Entry<String, Integer>> list;

    /**
     * Get the number of words.
     * @return the number of words
     */
    public long getWordsCount() {
        return wordsCount;
    }

    /**
     * Increase the number of words.
     * @param value
     */
    public void addWordsCount(final long value) {
        wordsCount += value;
    }

    /**
     * Get the number of characters.
     * @return long value
     */
    public long getCharsCount() {
        return charsCount;
    }

    /**
     * Increase the number of characters.
     * @param value
     */
    public void addCharsCount(final long value) {
        charsCount +=  value;
    }

    /**
     * Get the number of characters without spaces.
     * @return long value
     */
    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    /**
     * Increase the number of characters without spaces.
     * @param value
     */
    public void addCharsCountWithoutSpaces(final long value) {
        charsCountWithoutSpaces += value;
    }

    /**
     * Get the number of punctuation marks.
     * @return long value
     */
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Increase the number of punctuation marks.
     * @param value
     */
    public void addCharsCountOnlyPunctuations(final long value) {
        charsCountOnlyPunctuations += value;
    }

    /**
     * Method for finding top10 of most popular words in the text.
     * @return List value
     */
    public List<String> getTopWords() {

        List<String> simpleList = new ArrayList<>();

        for (Map.Entry<String, Integer> entry : getTop10Words()) {
            simpleList.add(entry.getKey());
        }
        return simpleList;

    }

    /**
     * Method for getting sorted List of words.
     * Words will be sorted in descending order of occurrence.
     * @return ArrayList value
     */
    public List<Map.Entry<String, Integer>> getTop10Words() {
        return list.subList(0, ((list.size() < NUMBER_OF_WORDS)
                                    ? list.size()
                                    : NUMBER_OF_WORDS));
    }

    /**
     * Method for getting sorted List of words of the text.
     * Words will be presented in descending order of occurrence.
     * @return ArrayList value
     */
    public List<Map.Entry<String, Integer>> getTop10WordsWithoutPrepositions() {
        List<Map.Entry<String, Integer>> entries =
                new ArrayList<>();

        int i = 0;
        int iterator = 0;


        while (i < ((list.size() < NUMBER_OF_WORDS)
                        ? list.size()
                        : NUMBER_OF_WORDS)) {
            if (list.get(iterator).getKey().length() > 2) {
                entries.add(list.get(iterator));
                i++;
            }
            iterator++;
        }

        return entries;

    }

    /**
     * Setting the sorted List of all words in the text.
     * Words will be presented in descending order of occurrence.
     * @param listValue
     */
    public void addAllSortedWords(
            final List<Map.Entry<String, Integer>> listValue) {
        this.list = listValue;
    }

    /**
     * Getting the HashMap table of all words in the text.
     * @return HashMap table
     */
    public Map<String, Integer> getWordsMap() {
        return map;
    }

    /**
     * Setting the HashMap collection of all words in the text.
     * @param mapValue
     */
    public void addWordsMap(final Map<String, Integer> mapValue) {
        this.map = mapValue;
    }

    /**
     * Text representation.
     * @return text
     */
    @Override
    public String toString() {
        return "TextStatistics {"
                + "wordsCount = " + wordsCount
                + ", charsCount = " + charsCount
                + ", charsCountWithoutSpaces = " + charsCountWithoutSpaces
                + ", charsCountOnlyPunctuations = " + charsCountOnlyPunctuations
                + '\n' + list
                + '\n' + getTop10WordsWithoutPrepositions()
                + '\n' + getTop10Words()
                + '\n' + getTopWords()
                + "}";

    }
}
