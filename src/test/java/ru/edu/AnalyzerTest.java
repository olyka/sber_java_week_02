package ru.edu;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AnalyzerTest {

    public static final String FILE_PATH = "./src/test/resources/input_text.txt";
    public static final int EXPECTED_WORDS = 983;
    public static final int EXPECTED_CHARS = 6651;
    public static final int EXPECTED_CHARS_WO_SPACES = 5508;
    // [^\s0-9a-zA-Zа-яА-я]{1}
    public static final int EXPECTED_CHARS_ONLY_PUNCT = 269;
    public static final String RESULT_PATH = "./result.txt";

    private final TextAnalyzer analyzer = new SimpleTextAnalyzer();
    private final SourceReader reader = new FileSourceReader();
    private final StatisticReporter reporter = new SimpleStatisticReporter(RESULT_PATH);


    @Test
    public void basicAnalyzerValidation() {
        reader.setup(FILE_PATH);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);
        System.out.println(statistics);

        reporter.report(statistics);
        File file = new File(RESULT_PATH);
        assertEquals(true, file.exists());

     }

     @Test
     public void analyzerValidation() {
        reader.setup(FILE_PATH);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        assertEquals(EXPECTED_WORDS, statistics.getWordsCount());
        assertEquals(EXPECTED_CHARS_WO_SPACES, statistics.getCharsCountWithoutSpaces());
        assertEquals(EXPECTED_CHARS_ONLY_PUNCT, statistics.getCharsCountOnlyPunctuations());
        assertEquals(EXPECTED_CHARS, statistics.getCharsCount());

        reporter.report(statistics);
        File file = new File(RESULT_PATH);
        assertEquals(true, file.exists());

    }

    @Test
    public void topWordsBasicValidation() {

        reader.setup(FILE_PATH);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);
        assertEquals(10, statistics.getTop10WordsWithoutPrepositions().size());
        assertEquals(10, statistics.getTop10Words().size());
        assertEquals(10, statistics.getTopWords().size());

    }

     @Test
     public void topWordsValidation() {

         reader.setup(FILE_PATH);
         TextStatistics statistics = reader.readSource(analyzer);

         assertNotNull(statistics);

         List<Map.Entry<String, Integer>> list =
                 statistics.getTop10WordsWithoutPrepositions();

             assertEquals("она", list.get(0).getKey());
             assertEquals(19, list.get(0).getValue().intValue());

             assertEquals("это", list.get(1).getKey());
             assertEquals(13, list.get(1).getValue().intValue());

             assertEquals("было", list.get(2).getKey());
             assertEquals(11, list.get(2).getValue().intValue());

             assertEquals("был", list.get(3).getKey());
             assertEquals(8, list.get(3).getValue().intValue());

             assertEquals("как", list.get(4).getKey());
             assertEquals(8, list.get(4).getValue().intValue());

             assertEquals("все", list.get(5).getKey());
             assertEquals(7, list.get(5).getValue().intValue());

             assertEquals("для", list.get(6).getKey());
             assertEquals(7, list.get(6).getValue().intValue());

             assertEquals("меня", list.get(7).getKey());
             assertEquals(7, list.get(7).getValue().intValue());

             assertEquals("что", list.get(8).getKey());
             assertEquals(7, list.get(8).getValue().intValue());

             assertEquals("еще", list.get(9).getKey());
             assertEquals(6, list.get(9).getValue().intValue());

     }

}