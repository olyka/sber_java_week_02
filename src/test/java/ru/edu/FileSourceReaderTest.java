package ru.edu;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FileSourceReaderTest {

    private final static String TEST_SOURCE_READER = "./src/test/resources/test_source_reader.txt";
    private final static String NOT_EXIST_FILE = "./not_exist_file/";

    private SourceReader reader = new FileSourceReader();

    @Test (expected = IllegalArgumentException.class)
    public void setupNull() {
        reader.setup(null);
    }

    @Test
    public void setupNotEmptyLine() {
        reader.setup(TEST_SOURCE_READER);
    }

    @Test (expected = IllegalArgumentException.class)
    public void setupNotExistFile() {
        reader.setup(NOT_EXIST_FILE);
    }

    @Test
    public void readSource() {
        TestTextAnalyzer analyzer = new TestTextAnalyzer();

        reader.setup(TEST_SOURCE_READER);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        assertEquals(2, statistics.getWordsCount());

        assertEquals("first line", analyzer.lines.get(0));
        assertEquals("second line", analyzer.lines.get(1));
    }


    static class TestTextAnalyzer implements TextAnalyzer {

        public List<String> lines  = new ArrayList<>();

        /**
         * Анализ строки произведения.
         *
         * @param line
         */
        @Override
        public void analyze(String line) {
            lines.add(line);
        }

        /**
         * Получение сохраненной статистики.
         *
         * @return TextStatistic
         */
        @Override
        public TextStatistics getStatistic() {
            TextStatistics statistics = new TextStatistics();
            statistics.addWordsCount(lines.size());
            return statistics;
        }
    }

}