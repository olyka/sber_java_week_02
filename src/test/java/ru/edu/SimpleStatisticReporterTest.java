package ru.edu;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class SimpleStatisticReporterTest {

    public static final String RESULT_TXT = "./src/test/resources/reporter.txt";

    @Test
    public void writeStatus() {
        StatisticReporter reporter = new SimpleStatisticReporter(RESULT_TXT);

        TextStatistics statistics = new TextStatistics();

        statistics.addCharsCount(100);
        statistics.addWordsCount(50);
        statistics.addCharsCountWithoutSpaces(25);
        statistics.addCharsCountOnlyPunctuations(10);
        statistics.addWordsMap(new HashMap<>());
        statistics.addAllSortedWords(new ArrayList<>());

        reporter.report(statistics);

        List<String> lines = readFiLe(RESULT_TXT);
//          количество строк
        assertEquals(7, lines.size());
        assertEquals("Words: 50", lines.get(0));
        assertEquals("Chars: 100", lines.get(1));
        assertEquals("Chars without spaces: 25", lines.get(2));
        assertEquals("Punctuation chars: 10", lines.get(3));

    }

    private List<String> readFiLe(String resultTxt) {

        List<String> lines = new ArrayList<>();
        try (
                FileReader fr = new FileReader(new File(resultTxt));
        ) {
            BufferedReader br = new BufferedReader(fr);
            String line;
            while((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }


}
