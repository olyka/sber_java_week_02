package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleTextAnalyzerTest {

    @Test
    public void analyze() {
        TextAnalyzer analyzer = new SimpleTextAnalyzer();

        analyzer.analyze("12345");
        TextStatistics statistic = analyzer.getStatistic();
        assertEquals(1, statistic.getWordsCount());
        assertEquals(5, statistic.getCharsCount());
        assertEquals(5, statistic.getCharsCountWithoutSpaces());
        assertEquals(0, statistic.getCharsCountOnlyPunctuations());

    }

    @Test
    public void analyzeMultiply() {
        TextAnalyzer analyzer = new SimpleTextAnalyzer();

        analyzer.analyze("12345");
        analyzer.analyze("\n\n123, asd45");
        TextStatistics statistic = analyzer.getStatistic();

        System.out.println(statistic);

        assertEquals(3, statistic.getWordsCount());
        assertEquals(17, statistic.getCharsCount());
        assertEquals(16, statistic.getCharsCountWithoutSpaces());
        assertEquals(1, statistic.getCharsCountOnlyPunctuations());

    }

}